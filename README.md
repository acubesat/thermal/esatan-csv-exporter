# ESATAN-TMS to CSV converter

This is a small tool which creates a CSV file from the contents of an
ESATAN-TMS result file (*.d)

For now, only nodes ($NODES) and material properties ($LOCALS) are supported.
The tool works normally and will evolve (function-wise) as needs arise. However,
a good cleanup of the code is in order (it's a quickly-made script after all)


# Installation

Depends on Python 3.7 and the [Gooey](https://github.com/chriskiehl/Gooey/) library.
The recommended way to installing the tool's dependencies is by using
[poetry](https://python-poetry.org/).
Just run `poetry install` while inside the repository's root directory.

**NOTE:** `wxpython` will take quite a bit of time to be installed on Linux, 
due to compilation in the background.
This is normal behavior.

# Distribution

`pyinstaller` is included as a dev dependency. 
For Windows, a suitable `build.spec` file is provided at the project root (tested on Windows 10
with the dependencies stated in `poetry.lock`).
Run `poetry run pyinstaller -F --windowed build.spec` and the resulting executable
will be residing in a new `dist/` folder inside this repository.

# Screenshot

![image](/uploads/1deb29260db7e4b7a641360909f228ce/image.png)

# License

GNU GPL v3.
