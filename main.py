from src.parser import *
from src.sanitizer import *

from itertools import islice
from gooey import Gooey, GooeyParser

import csv

def _calculate_c(node_properties: dict, variables: dict) -> float:
    c_expression = node_properties['C'].split('*')
    # substitute D for E for float() to work
    c_expression[0] = c_expression[0].replace('D', 'E')
    variable_product = 1
    # first element is already known, others are variables for substitution
    for variable in c_expression[1:]:
        variable_product = variable_product * variables[variable]
    return float(c_expression[0]) * variable_product

@Gooey(program_name="ESATAN CSV Exporter")
def main():
    cmd_parser = GooeyParser(description="Export result files to CSV")
    cmd_parser.add_argument("--input", help="input file to be converted", widget="FileChooser")
    cmd_parser.add_argument("--output", help="path of output file", widget="FileSaver")

    args = cmd_parser.parse_args()

    variable_section, node_section = "", ""

    # TODO: find a better method to detect sections, this one sucks
    with open(args.input, 'r') as file:
        local_start, node_start, node_end = 0, 0, 0
        for line_no, line in enumerate(file):
            if "$LOCALS" in line:
                local_start = line_no
            elif "$NODES" in line:
                node_start = line_no
            elif "$CONDUCTORS" in line:
                node_end = line_no
    
    with open(args.input, 'r') as file:
        for line in islice(file, local_start, node_start):
            variable_section += line
        for line in islice(file, 0, node_end-node_start):
            node_section += line
        
    # sanitize sections
    variable_section = sanitize(variable_section)
    node_section = sanitize(node_section)

    # parse node and variable sections
    variables = parse_material_properties(variable_section)
    nodes = parse_nodes(node_section)


    # calculate C and add the key with 0 if missing
    for node in nodes:
        if 'C' in node:
            node['C'] = _calculate_c(node, variables)
        else:
            node['C'] = 0

        if 'A' in node:
            node['A'] = float(node['A'])
        else:
            node['A'] = 0
            

    # write to CSV
    with open(args.output, 'w') as csvfile:
        columns = ["D", "NAME", "A", "C"]
        writer = csv.DictWriter(csvfile, fieldnames=columns)
        writer.writeheader()

        for node in nodes:
            writer.writerow({"D": node["D"],"NAME": node["NAME"], "A":float(node["A"]), "C": node["C"]})

if __name__ == "__main__":
    main()