#from sanitizer import sanitize

def parse_material_properties(source: str) -> dict:
    """
    Converts the constant section of the input file to a dict with
    the variables for further calculation (e.g. for the C variable in nodes)
    """
    material_properties: dict = {}
    assignment_list: list = [expr for expr in source.split(';') if expr != ""]
    material_properties.update([_parse_single_assignment(expr) for expr in assignment_list])

    for k, v in material_properties.items():
        material_properties[k] = float(v)

    return material_properties


def parse_nodes(source: str) -> list:
    node_properties_raw: list = [prop for prop in source.split(';') if prop != ""]
    node_properties: list = []
    for raw_node in node_properties_raw:
        assignment_list = _parse_multiple_assignment(raw_node)

        # D value is needed as separate element, so parsing is done in 2 stages
        d_value_raw, name = assignment_list.pop(0)
        d_value = _parse_d_value(d_value_raw)
        
        parsed_node: dict = {}
        parsed_node.update({"D": d_value})
        parsed_node.update({"NAME": name})
        parsed_node.update(assignment_list)
        
        node_properties.append(parsed_node)
    return node_properties


def _parse_single_assignment(source: str) -> tuple:
    """
    Parses a single assignment expression (of "name" = value format) to a
    (name, value) tuple.
    Note: Sanitize the input before calling.
    """
    expr_parts = source.split("=")
    # 0: name, 1: value
    return (expr_parts[0], expr_parts[1])


def _parse_multiple_assignment(source: str) -> list:
    exprs = source.split(',')
    return [_parse_single_assignment(e) for e in exprs]


def _parse_d_value(source: str) -> int:
    if source.startswith("D"):
        return int(source[1:])



if __name__ == "__main__":
    test = """
     D4 = 'MQTR2', T = 0.D+00,
     C = 1.444504D-06 * Cp_copper * Dens_copper,
     A = 0.000903, ALP = 1.000000, EPS = 0.800000,
     FX = -0.0350000, FY = 0.0847500, FZ = 0.000250000;
    D2544 = 'wallttouniunder1', T = 0.D+00,
         FX = -0.0305000, FY = -0.0414200, FZ = 0.000000;
        D2545 = 'wallttouniunder1', T = 0.D+00,
         FX = -0.0305000, FY = -0.0444200, FZ = 0.000000;
        D2546 = 'wallttouniunder1', T = 0.D+00,
         FX = -0.0305000, FY = -0.0429200, FZ = 0.00550000;
        D2547 = 'wallttouniunder1', T = 0.D+00,
         FX = -0.0305000, FY = -0.0429200, FZ = -0.00550000;
        D2548 = 'wallttouniunder1', T = 0.D+00,
         FX = -0.0275000, FY = -0.0429200, FZ = 0.000000;
        D2549 = 'wallttouniunder1', T = 0.D+00,
         FX = -0.0335000, FY = -0.0429200, FZ = 0.000000;
        D2550 = 'wallttouniunder2', T = 0.D+00,
         C = 1.98D-07 * Cp_Al * Dens_Al,
         FX = 0.0305000, FY = -0.0429200, FZ = 0.000000;
        D2551 = 'wallttouniunder2', T = 0.D+00,
         FX = 0.0305000, FY = -0.0414200, FZ = 0.000000;
        D2552 = 'wallttouniunder2', T = 0.D+00,
         FX = 0.0305000, FY = -0.0444200, FZ = 0.000000;
        D2553 = 'wallttouniunder2', T = 0.D+00,
         FX = 0.0305000, FY = -0.0429200, FZ = -0.00550000;
        D2554 = 'wallttouniunder2', T = 0.D+00,
         FX = 0.0305000, FY = -0.0429200, FZ = 0.00550000;
        D2555 = 'wallttouniunder2', T = 0.D+00,
         FX = 0.0275000, FY = -0.0429200, FZ = 0.000000;
        D2556 = 'wallttouniunder2', T = 0.D+00,
         FX = 0.0335000, FY = -0.0429200, FZ = 0.000000;
    """
    test = sanitize(test)

    l = parse_nodes(test)

    for i in l:
        print(i)