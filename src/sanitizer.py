from re import sub

def sanitize(source: str) -> str:
    return _strip_whitespace(_strip_comments(_strip_headers(source)))

def _strip_comments(source: str) -> str:
    return sub(r'(?m)^ *#.*\n?', '', source)

def _strip_whitespace(source: str) -> str:
    return "".join(source.split())

def _strip_headers(source: str) -> str:
    return sub(r'\$.*', '', source)
    